//Representation des Interfaces de Props et State

export interface IProps {
    route?: any;
    capsule?: any;
    detailCap?: (elt: any) => any;
    navigation?: any;
}

export interface IState {
    capsules: any;
    isLoading: any;
}