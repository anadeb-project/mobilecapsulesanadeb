import * as React from 'react';
import { Button, Card, Title, Paragraph } from 'react-native-paper';
import { View, StyleSheet, Text } from 'react-native';
import { IProps } from '../Interfaces/Types';

class CapsuleItem extends React.Component<IProps> {
    //Component pour representer une capsule

    public capsule: any;

    constructor(props: any){
        super(props)
        this.capsule = this.props.capsule; //recuperation de la capsule passée dans le props
    }
    paragraph(){
        //Methode retournant les details s'ils existent, sinon, elle retourne le type
        let details = this.capsule.details;
        if(details == null || details == undefined){
            return(
                <View style={styles.type_label_container}>
                    <Text style={styles.type_label}></Text>
                    <Text style={styles.type}>Type : {this.capsule.type}</Text>
                 </View>
            )
        }
        return(
            <View>
                <Text style={styles.details}>Details : {((details).length > 35) ? (((details).substring(0,35)) + '...') : details}</Text>
             </View>
        )
    }
    render(){
        
        const detailCap = this.props.detailCap == undefined ? function(elt: any){return elt;} : this.props.detailCap;

        return(
            <View style={styles.container} >
                
                <Card>
                    <Card.Content>
                        <View>
                            <Title>{this.capsule.capsule_serial}</Title>
                        </View>
                        <View style={styles.status_container}>
                            <Paragraph><Text>{this.capsule.status}</Text></Paragraph>
                        </View>
                        <View style={styles.star_container}>
                            <Paragraph><Text>
                            
                            { this.paragraph() }

                            </Text></Paragraph>
                        </View>
                    </Card.Content>
                    <Card.Actions>
                        <Button onPress={() => detailCap(this.capsule)} >Voir plus</Button>
                    </Card.Actions>
                </Card>
            
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
    container: {
        marginBottom: 20,
    },
    status_container: {
        alignItems: 'flex-end',

    },
    star_container : {
        alignItems: 'center',
    },
    type_label_container: {
        flex: 1,
    },
    type_label: {
        flex: 1,
        alignItems: 'flex-start',
        textDecorationLine: 'line-through', 
        textDecorationStyle: 'solid'
    },
    type: {
        flex: 1,
        alignItems: 'flex-end',
        color: 'green'
    },
    details: {
        color: 'green'
    }
  });

  

export default CapsuleItem;