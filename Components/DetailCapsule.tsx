import * as React from 'react';
import { Avatar, Card, Title, Paragraph } from 'react-native-paper';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import { IProps } from '../Interfaces/Types';


class DetailCapsule extends React.Component<IProps> {
    //Component pour representer la page de detail de la capsule

    private capsule: any;
    constructor(props: any){
        super(props)
        const { capsule } = this.props.route.params; // recuperation de la capsule passee en parametre
        this.capsule = capsule;
    }

    _formatDate(date: Date): string
    {
        //Methode permettamt de faire le formatage d'une date
        return date.toLocaleDateString('en-us', { weekday:"long", year:"numeric", month:"short", day:"numeric", hour:"numeric", minute:"numeric"}) ;
    }

    _missions(datas: any){
        //Methode permettant de lister les missions d'une capsule
        if(datas && datas.length >= 1){
            return(
                <View>
                    <Title>Mission{datas.length > 1 ? "s" : "" }</Title>
                    <FlatList
                        data={datas}
                        keyExtractor={(item) => item.name.toString()}
                        onEndReachedThreshold={0.5}
                        onEndReached={() => {
                            
                        }}
                        renderItem={
                            ({item, index}) => {
                            return(
                                <Text>{index + 1}-/ Nom : {item.name} ; Nombre de voyage en avion : {item.flight}</Text>
                            );
                            } 
                        }
                        />
                </View> 
               
            );
        }else{
            return(
                <View>
                    <Title>Missions : Pas de missions</Title>
                </View>
            );
        }
        
    }
    
    render(){
        const original_launch = this.capsule.original_launch ? this._formatDate(new Date(this.capsule.original_launch)) : "Pas definie";
        const original_launch_unix = this.capsule.original_launch_unix ? this._formatDate(new Date(1000*this.capsule.original_launch_unix)) : "Pas definie";
        return(
            <Card>
                
                
                <Card.Content>
                    <View style={styles.title_container}>
                        <Title style={styles.title}>{this.capsule.capsule_serial}</Title>
                    </View>
                    <View>
                        <Paragraph style={styles.view}>Status : {this.capsule.status}</Paragraph>
                    </View>
                    <View>
                        <Paragraph style={styles.view}>Date de lancement : {original_launch}</Paragraph>
                    </View>
                    <View>
                        <Paragraph style={styles.view}>Date d'initiation : {original_launch_unix}</Paragraph>
                    </View>
                    <View>
                        <Paragraph style={styles.view}>Nombre d'Atterrissages : {this.capsule.landings}</Paragraph>
                    </View>
                    <View>
                        <Paragraph style={styles.view}>Type : {this.capsule.type}</Paragraph>
                    </View>
                    <View>
                        <Paragraph style={styles.view}>Nombre de réutilisations : {this.capsule.reuse_count}</Paragraph>
                    </View>
                    <View>
                        <Paragraph style={styles.view}>Details : {this.capsule.details ? this.capsule.details : "Pas de details"}</Paragraph>
                    </View>
                    <View>
                        <View><Text></Text></View>
                        <View><Text></Text></View>
                        <View><Text></Text></View>
                        <View><Text></Text></View>
                        <View><Text></Text></View>
                        <View><Text></Text></View>
                        <Paragraph style={styles.view}>
                           {this._missions(this.capsule.missions)}
                        </Paragraph>
                    </View>
                </Card.Content>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    
    title_container: {
        alignItems: 'center',
    },
    title : {
        alignItems: 'center',
    },
    view : {
        fontSize: 15
    },
  });

  

export default DetailCapsule;