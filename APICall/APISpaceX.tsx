import { URL_BASE } from '../environnement';


export function getAllCapsules(){
    //Fonction pour recuperer la liste des capsules
    const url = URL_BASE + "capsules/";
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.log(error))
}