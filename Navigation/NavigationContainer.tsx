import ListCapsule from '../Components/ListCapsule';
import DetailCapsule from '../Components/DetailCapsule';

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//Mis en place de la navigation entre le component de la liste des capsules et le component du detailn d'une capsule

const Stack = createStackNavigator();

const NavigateCapsuleToDetail = () => {
  return (
    <NavigationContainer initialRouteName="Home">
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={ListCapsule}
          options={{ title: 'Capsules' }}

        />
        <Stack.Screen name="Detail de la Capsule" component={DetailCapsule} />
      </Stack.Navigator>
    </NavigationContainer>
  );

};
export default NavigateCapsuleToDetail;